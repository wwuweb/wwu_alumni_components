<?php
/**
 * @file
 * wwu_alumni_components.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_alumni_components_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
